# Docker Minio Backup

Docker image for running backups of an S3-compatible bucket to local storage.o

## Config

Environment variables:

 - `$MINIO_SERVER_URL` - S3 server endpoint. Example: `https://minio.example.com`
 - `$MINIO_BUCKET` - Bucket name to mirror
 - `$MINIO_ACCESS_KEY` - Access key for the bucket
 - `$MINIO_SECRET_KEY` - Secret key for the bucket

Optional environment variables:

 - `$SLEEP_TIME` - Number of seconds to sleep between copy jobs, to allow container
   to stay running. Set to `0` to run in batch-job mode and exit after one copy. Defaults
   to 4 hours if not set.

Volumes:
 - mount a volume to `/backup` where the bucket will be mirrored.

## Container Images

The following images are build by CI:

On tags for releases:
 - `registry.gitlab.com/benyanke/docker-minio-backup:latest`
 - `registry.gitlab.com/benyanke/docker-minio-backup:{tag}`

On every push to branches:
 - `registry.gitlab.com/benyanke/docker-minio-backup:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-minio-backup:commit_{commit_hash}`

Note that the `branch_*` and `commit_*` tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing. If it works, make a git tag
matching the pattern `vX.X.X` where `X` is an integer.
