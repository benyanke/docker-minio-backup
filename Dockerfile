FROM minio/mc

ADD backup.sh /usr/bin/backup.sh

# By default, sleep 4 hours (in seconds) before running again
# Setting to zero will force the container to simply run and then exit
ENV SLEEP_TIME 14400
ENV LOCAL_BACKUP_PATH /backup

ENTRYPOINT []
CMD ["/bin/bash","/usr/bin/backup.sh"]
