#!/bin/bash

set -e

# DEFAULT ENVS SET IN DOCKERFILE

#######
# Validate incoming config
#######

REQUIRED_ENVS="SLEEP_TIME LOCAL_BACKUP_PATH MINIO_SERVER_URL MINIO_BUCKET MINIO_ACCESS_KEY"
# These are validated but values not logged to stdout
REQUIRED_SECRET_ENVS="MINIO_SECRET_KEY"

echo "Validating config"
export FAIL=0
for e in $REQUIRED_ENVS ; do
  echo " - $e = ${!e}"
  if [[ "${!e}" == "" ]] ; then
    FAIL=1
  fi
done
for e in $REQUIRED_SECRET_ENVS ; do
  if [[ "${!e}" == "" ]] ; then
    echo " - $e = "
    FAIL=1
  else
    echo " - $e = *********"
  fi
done

if [[ "$FAIL" == "1" ]] ; then
  echo "Validation errors exist, all variables above must not be blank - can not continue"
  exit 1
fi

#######
# Run backup
#######

mc alias set server "${MINIO_SERVER_URL}" "${MINIO_ACCESS_KEY}" "${MINIO_SECRET_KEY}"
mkdir -p "${LOCAL_BACKUP_PATH}"
cd "${LOCAL_BACKUP_PATH}"

while true ; do

  echo "Beginning backup of $MINIO_SERVER_URL / $MINIO_BUCKET"

  mc mirror --overwrite --remove "server/${MINIO_BUCKET}" "${LOCAL_BACKUP_PATH}"

  # TODO : look into adding --watch support

  echo
  if [[ "$SLEEP_TIME" == "0" ]] ; then
    echo "Container configured in job-runner mode, backup complete, exiting."
    exit 0;
  else
    echo "Container configured in persistent mode - sleeping $SLEEP_TIME seconds then running again."
    sleep $SLEEP_TIME
  fi

done
